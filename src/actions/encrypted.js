import { Communicator } from "@8ocorg/communicator";

const getEncrypted = () => {
  let comm = new Communicator(["main"]);
  return comm.main.get("encrypted");
};

export { getEncrypted };
