import React from "react";

import {
  IcosahedronContainer,
  icosahedronTriangles,
} from "assets/styles/Icosahedron.styles.jsx";

const Icosahedron = () => (
  <IcosahedronContainer>
    {icosahedronTriangles.map((IcosahedronTriangle, i) => (
      <IcosahedronTriangle key={i} />
    ))}
  </IcosahedronContainer>
);

export default Icosahedron;
