import React from "react";

import { AppContainer, AppStatic } from "assets/styles/App.styles.jsx";

import Blink from "components/Blink.jsx";
import Icosahedron from "components/Icosahedron.jsx";

const App = () => {
  return (
    <AppContainer>
      <AppStatic />
      <Blink />
      <Icosahedron />
    </AppContainer>
  );
};

export default App;
