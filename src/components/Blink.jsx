import React, { useState, useEffect } from "react";

import { BlinkHeader } from "assets/styles/Blink.styles.jsx";

import { getEncrypted } from "actions/encrypted";
import { toMorseCode } from "actions/morse";

const TICK_SPEED = 40; // how often to produce a new element in miliseconds
const UNIT_LENGTH = 6; // how long one unit of morse is in px
const UNIT_AMOUNT = 12; // how many 404s one unit gives

// to international morse code standards https://en.wikipedia.org/wiki/Morse_code#/media/File:International_Morse_Code.svg
const DOT_AMOUNT = UNIT_AMOUNT;
const DASH_AMOUNT = UNIT_AMOUNT * 3;
const INTRA_LETTER_SPACE_AMOUNT = UNIT_AMOUNT;
const INTER_LETTER_SPACE_AMOUNT = UNIT_AMOUNT * 3;
// we should never have more than one word

const Blink = () => {
  const [morseString, setMorseString] = useState("");

  const [blinkArray, setBlinkArray] = useState([0]);
  const [tick, setTick] = useState(0);
  const [tickInterval, setTickInterval] = useState(() => {});
  const [trigger, setTrigger] = useState(1);
  const [morseIndex, setMorseIndex] = useState(0);

  useEffect(() => {
    // Promise.resolve("3afn4n9fnanfjg").then((encrypted) => {
    getEncrypted().then((encrypted) => {
      // console.log(encrypted);
      setMorseString(toMorseCode(encrypted));
    });
  }, []);

  useEffect(() => {
    if (morseString) {
      setTickInterval(
        setInterval(() => {
          setBlinkArray((prevBlinkArray) =>
            prevBlinkArray
              .map((e) => e + UNIT_LENGTH)
              .filter((e) => e < window.screen.height)
          );
          setTick((prevTick) => prevTick + 1);
          setTrigger((prevTrigger) => prevTrigger - 1); // needs to be one because this resolves before tick effect
        }, TICK_SPEED)
      );

      return () => clearInterval(tickInterval);
    }
  }, [morseString]);

  useEffect(() => {
    if (!morseString) {
      return;
    }

    if (morseIndex > morseString.length) {
      setTrigger(Number.NEGATIVE_INFINITY);
    }

    if (morseIndex > morseString.length + window.screen.height / UNIT_LENGTH) {
      clearInterval(tickInterval);
    }

    if (trigger === 0) {
      switch (morseString[morseIndex]) {
        case ".":
          setBlinkArray((prevBlinkArray) => [
            ...prevBlinkArray,
            ...Array(DOT_AMOUNT)
              .fill(0)
              .map((e, i) => e - i * UNIT_LENGTH),
          ]);
          setTrigger(DOT_AMOUNT + INTRA_LETTER_SPACE_AMOUNT);
          break;
        case "-":
          setBlinkArray((prevBlinkArray) => [
            ...prevBlinkArray,
            ...Array(DASH_AMOUNT)
              .fill(0)
              .map((e, i) => e - i * UNIT_LENGTH),
          ]);
          setTrigger(DASH_AMOUNT + INTRA_LETTER_SPACE_AMOUNT);
          break;
        default:
          setBlinkArray((prevBlinkArray) => [
            ...prevBlinkArray,
            ...Array(INTER_LETTER_SPACE_AMOUNT)
              .fill(Number.NEGATIVE_INFINITY)
              .map((e, i) => e - i * UNIT_LENGTH),
          ]);
          setTrigger(INTER_LETTER_SPACE_AMOUNT);
          break;
      }

      setMorseIndex((prevMorseIndex) => prevMorseIndex + 1);
    }
  }, [tick]);

  return blinkArray
    .filter((e) => e >= 0)
    .map((e, i) => (
      <BlinkHeader top={e} key={i + 1}>
        404
      </BlinkHeader>
    ));
};

export default Blink;
