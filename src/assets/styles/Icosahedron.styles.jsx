import styled, { keyframes } from "styled-components";

const triangleColor = "black";
const triangleSide = "198px";
const angle = 360 / 5;

const spin = keyframes`
  from { transform: rotateY(0deg) rotateX(0deg); }
  to { transform: rotateY(360deg) rotateX(360deg); }
`;

const IcosahedronContainer = styled.div`
  position: absolute;
  width: ${triangleSide};
  height: ${(Math.sqrt(3) * parseFloat(triangleSide)) / 2}px;
  top: 30%;
  right: 20%;

  transform-style: preserve-3d;
  // outline: 1px solid lavender;
  animation: ${spin} 5s linear infinite;
`;

const translateZVal =
  (parseFloat(triangleSide) * (3 * Math.sqrt(3) + Math.sqrt(15))) / 12;

// https://mae.ufl.edu/~uhk/ICOSAHEDRON.pdf
const girthHeightRatio = 0.850650808; // d

// pentagonal inradius - the cap formes a pentagon
const pentagonInradius = parseFloat(triangleSide) / (2 * Math.tan(0.628319));

console.log(pentagonInradius);

const icosahedronTriangles = Array.from(
  { length: 21 },
  (_, i) => styled.div`
    position: absolute;
    top: 0px;
    width: 0px;
    height: 0px;
    border-left: ${parseFloat(triangleSide) / 2}px solid transparent;
    border-right: ${parseFloat(triangleSide) / 2}px solid transparent;
    border-bottom: ${(Math.sqrt(3) * parseFloat(triangleSide)) / 2}px solid
      ${triangleColor};
    // opacity: .4;
    transition: all 0.5s ease;

    // top part
    ${i < 5 &&
    `
		transform: rotateY(-${
      angle * (i + 1)
    }deg) translateZ(-${pentagonInradius}px) rotateX(-53deg);
		transform-origin: bottom center;
		top: -${parseFloat(triangleSide) * girthHeightRatio}px;
		${IcosahedronContainer}:hover & {
			top: -${parseFloat(triangleSide)}px;
			transform: rotateY(-${angle * (i + 1)}deg) translateZ(-${
      pentagonInradius * 2
    }px) rotateX(-53deg);
		}
	`}

    // bottom part (inversion of top)
	${i >= 5 &&
    i < 10 &&
    `
		transform: rotateY(-${
      angle * (i + 1)
    }deg) translateZ(${pentagonInradius}px) rotateX(-53deg);
		top: ${parseFloat(triangleSide) * girthHeightRatio}px;
		transform-origin: top center;
		border-bottom: none;
		border-top: ${
      (Math.sqrt(3) * parseFloat(triangleSide)) / 2
    }px solid ${triangleColor};
		${IcosahedronContainer}:hover & {
			top: ${parseFloat(triangleSide)}px;
			transform: rotateY(-${angle * (i + 1)}deg) translateZ(${
      pentagonInradius * 2
    }px) rotateX(-53deg);
		}
	`}

	// tilt = compliment of tetrahedral angle (31.717deg) = 90 - 31.717 = 58.283
	// half of dihedral angle (138.189) is 69.0945
	// subtract tilt by half of dihedral angle = 58.283 - 69.0945 = -10.8115deg
	// this is our tilt for the rings

	// first ring
	${i >= 10 &&
    i < 15 &&
    `
		transform: rotateY(-${
      angle * (i + 1)
    }deg) translateZ(-${translateZVal}px) rotateX(-10.8115deg);
		top: 0;
		border-bottom: none;
		border-top: ${
      (Math.sqrt(3) * parseFloat(triangleSide)) / 2
    }px solid ${triangleColor};
		${IcosahedronContainer}:hover & {
			transform: rotateY(-${angle * (i + 1)}deg) translateZ(-${
      parseFloat(triangleSide) * Math.sqrt(3)
    }px) rotateX(-10.8115deg);
		}
	`}

	// second ring (inversion of first ring)
	${i >= 15 &&
    i < 20 &&
    `
		transform: rotateY(-${
      angle * (i + 1) + 252
    }deg) translateZ(-${translateZVal}px) rotateX(10.8115deg);
		top: 0;
		border-top: none;
		border-bottom: ${
      (Math.sqrt(3) * parseFloat(triangleSide)) / 2
    }px solid ${triangleColor};
		${IcosahedronContainer}:hover & {
			transform: rotateY(-${angle * (i + 1) + 252}deg) translateZ(-${
      parseFloat(triangleSide) * Math.sqrt(3)
    }px) rotateX(10.8115deg);
		}
	`}

	${i === 20 &&
    `
    	border-bottom-color: white;
    	opacity: 0.5;
	`}
  `
);

export { IcosahedronContainer, icosahedronTriangles };
