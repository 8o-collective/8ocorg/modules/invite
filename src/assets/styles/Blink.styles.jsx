import styled from "styled-components";

const BlinkHeader = styled.div.attrs(({ top }) => ({
  style: {
    transform: `translateY(${top}px)`,
  },
}))`
  margin: 0px;
  position: absolute;
  top: 10vh;
  left: 10vw;

  color: white;
  font-family: "Courier Pixelated 32 Bold";
  font-size: max(5vw, 30px);
`;

export { BlinkHeader };
