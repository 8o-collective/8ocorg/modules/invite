import styled, { keyframes } from "styled-components";

import background from "assets/images/background.jpg";

const AppContainer = styled.div`
  background-image: url("${background}");
  background-size: cover;
  // background-size: 30%;

  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;

  overflow-y: hidden;
`;

const staticAnimation = keyframes`
  100% {background-position: 50% 0, 60% 50%};
`;

const AppStatic = styled.div`
  position: absolute;
  height: 20vh;
  width: 100%;
  left: 0px;
  bottom: 0px;

  background: repeating-radial-gradient(#000 0 0.0001%, #fff 0 0.0002%) 50% 0/2500px
      2500px,
    repeating-conic-gradient(#000 0 0.0001%, #fff 0 0.0002%) 60% 60%/2500px
      2500px;
  background-blend-mode: difference;
  animation: ${staticAnimation} 0.2s infinite alternate;
`;

export { AppContainer, AppStatic };
